# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

APPDIR = "/home/vagrant/workspace/rakulacook"
DBDIR  = "#{APPDIR}/db"
CSVDIR = "#{DBDIR}/csv"
LOG    = "#{DBDIR}/import.log"

if system("ls #{LOG} >& /dev/null")

  result = Dir.chdir(CSVDIR)

  File.open(LOG, "a") do |file|
    Dir.glob("*").each do |filename|
      if !system("grep #{filename} #{LOG} >& /dev/null")
        tbl = filename.split("_")
        sql = "LOAD DATA LOCAL INFILE '#{filename}' INTO TABLE #{tbl[1]} FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\r\n'"
        ActiveRecord::Base.connection.execute(sql)
      	file.puts(filename)
      end
    end
  end
else
  result = system("touch #{LOG} >& /dev/null")
  result = Dir.chdir(CSVDIR)

  File.open(LOG, "a") do |file|
    Dir.glob("*").each do |filename|
      if !system("grep #{filename} #{LOG} >& /dev/null")
        tbl = filename.split("_")
        sql = "LOAD DATA LOCAL INFILE '#{filename}' INTO TABLE #{tbl[1]} FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\r\n'"
        ActiveRecord::Base.connection.execute(sql)
      	file.puts(filename)
      end
    end
  end
end
