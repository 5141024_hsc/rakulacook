$(function(){
  var menu = $('.side'),
      menubtn = $('.sm_button'),
      body = $(document.body),
      menuWidth = menu.outerWidth();

      /*メニューボタンをクリックしたと気の動き*/
      menubtn.on('click',function(){
        body.toggleClass('open');
        if(body.hasClass('open')) {
          body.animate({
            'right' : menuWidth
          }, 300);
          menu.animate({
            'right' : 0
          }, 300);
        } else {
          menu.animate({
            'right' : -menuWidth
          }, 300);
          body.animate({
            'right' : 0
          }, 300);
        }
      });
});
