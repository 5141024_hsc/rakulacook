class Dish < ActiveRecord::Base
  belongs_to :genre
  has_and_belongs_to_many :users  #userと多対多関連
end
