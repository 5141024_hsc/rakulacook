Rails.application.routes.draw do
  ## 提案する献立の表示＆レシピの表示  2016/10/03追加　井上
  get 'dish' => 'recipe_pages#result'
  get 'recipe/:id' => 'recipe_pages#recipe'
  get 'test' => 'recipe_pages#test'
  ## end

  get 'recipe/:id/fav' => 'recipe_pages#fav'

  get 'manu_disp/menu_disp'

  post 'question/menu_disp' => 'manu_pages#menu_disp'
  post 'question'=>'manu_pages#question'

  get 'init'=>'manu_pages#init'

  get 'question'=>'manu_pages#question'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'signup' => 'user_pages#regist'

  get 'user_pages/:id' =>'user_pages#profile'

  get 'user_pages/:id/allergies' => 'user_pages#allergies'

  post 'user_pages/:id/allergies/allergiesinput' => 'user_pages#allergiesinput'

  get 'user_pages/:id/favdeleteselect' => 'user_pages#favdeleteselect'

  get 'user_pages/:id/favdeleteselect/favdelete' => 'user_pages#favdelete'

  get 'withdrawal' => 'user_pages#destroy'

  post 'signup/confirm' => 'user_pages#confirm'

  get 'about' =>'static_pages/about'

  root 'static_pages#home'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :user_pages
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]

end
